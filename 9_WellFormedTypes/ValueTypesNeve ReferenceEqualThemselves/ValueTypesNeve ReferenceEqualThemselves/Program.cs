﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValueTypesNeve_ReferenceEqualThemselves
{
    class Program
    {
        public struct coordinate
        { 
            public coordinate (Longitude longitude, Latitude latitude)
            {
                Longitude = longitude;
                latitude = latitude;
            }

            public Longitude Longitude { get; }
            public Latitude Latitude { get; }
        }

        public struct Longitude
        {
            public Longitude(int v1, int v2)
            {
            }
        }
        public struct Latitude
        {
            public Latitude(int v1, int v2)
            {
            }
        }
        static void Main()
        {
            coordinate coordinate1 = new coordinate(new Longitude (48, 52), new Latitude (-2, -20));

            if (coordinate.ReferenceEquals(coordinate1,coordinate1))
            {
                throw new Exception(
                "coordinate1 reference equals coordinate1");
            }
            Console.WriteLine("coordinate1 does not reference equal itself");
            Console.ReadKey();
        }
    }
}
