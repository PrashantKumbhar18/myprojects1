﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PEP369APP.Models;

namespace PEP369APP.Controllers
{
    public class tblResourcesController : Controller
    {
        private NewDBEntities1 db = new NewDBEntities1();

        // GET: tblResources
        public ActionResult Index()
        {
            return View(db.tblResources.ToList());
        }

        public ActionResult Pep()
        {
            return View(db.tblTasks.ToList());
        }

        // GET: tblResources/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblResource tblResource = db.tblResources.Find(id);
            if (tblResource == null)
            {
                return HttpNotFound();
            }
            return View(tblResource);
        }

        // GET: tblResources/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblResources/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ResourceID,SupID,ResourceName,Designation,Department,Address,Email,ContactNo,Role")] tblResource tblResource)
        {
            if (ModelState.IsValid)
            {
                db.tblResources.Add(tblResource);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblResource);
        }

        // GET: tblResources/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblResource tblResource = db.tblResources.Find(id);
            if (tblResource == null)
            {
                return HttpNotFound();
            }
            return View(tblResource);
        }

        // POST: tblResources/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ResourceID,SupID,ResourceName,Designation,Department,Address,Email,ContactNo,Role")] tblResource tblResource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblResource).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblResource);
        }

        // GET: tblResources/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblResource tblResource = db.tblResources.Find(id);
            if (tblResource == null)
            {
                return HttpNotFound();
            }
            return View(tblResource);
        }

        // POST: tblResources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblResource tblResource = db.tblResources.Find(id);
            db.tblResources.Remove(tblResource);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
