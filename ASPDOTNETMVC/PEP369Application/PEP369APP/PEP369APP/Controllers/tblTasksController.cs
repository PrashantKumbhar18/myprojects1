﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PEP369APP.Models;

namespace PEP369APP.Controllers
{
    public class tblTasksController : Controller
    {
        private NewDBEntities1 db = new NewDBEntities1();

        // GET: tblTasks
        public ActionResult Index()
        {
            return View(db.tblTasks.ToList());
        }

        // GET: tblTasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTask tblTask = db.tblTasks.Find(id);
            if (tblTask == null)
            {
                return HttpNotFound();
            }
            return View(tblTask);
        }

        // GET: tblTasks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblTasks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,EmpID,SupID,Objective,Description,Review,StartDate,EndDate,Status,Completed,Action,Comment,UpdateComment")] tblTask tblTask)
        {
            if (ModelState.IsValid)
            {
                db.tblTasks.Add(tblTask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblTask);
        }

        // GET: tblTasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTask tblTask = db.tblTasks.Find(id);
            if (tblTask == null)
            {
                return HttpNotFound();
            }
            return View(tblTask);
        }

        // POST: tblTasks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,EmpID,SupID,Objective,Description,Review,StartDate,EndDate,Status,Completed,Action,Comment,UpdateComment")] tblTask tblTask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblTask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblTask);
        }

        // GET: tblTasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblTask tblTask = db.tblTasks.Find(id);
            if (tblTask == null)
            {
                return HttpNotFound();
            }
            return View(tblTask);
        }

        // POST: tblTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblTask tblTask = db.tblTasks.Find(id);
            db.tblTasks.Remove(tblTask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
