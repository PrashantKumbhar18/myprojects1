//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PEP369APP.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblResource
    {
        public int ResourceID { get; set; }
        public int SupID { get; set; }
        public string ResourceName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> ContactNo { get; set; }
        public string Role { get; set; }
    }
}
