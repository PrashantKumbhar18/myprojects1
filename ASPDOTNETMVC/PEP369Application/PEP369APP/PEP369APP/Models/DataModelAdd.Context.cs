﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PEP369APP.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class NewDBEntities1 : DbContext
    {
        public NewDBEntities1()
            : base("name=NewDBEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tblResource> tblResources { get; set; }
        public virtual DbSet<tblSupervisor> tblSupervisors { get; set; }
        public virtual DbSet<tblTask> tblTasks { get; set; }
    }
}
